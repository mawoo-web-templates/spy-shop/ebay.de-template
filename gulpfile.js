var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var tinypng = require('gulp-tinypng-compress');
var htmlmin = require('gulp-htmlmin');

gulp.task('sass', function () {
    return gulp.src('./src/sass/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            // nested, expanded, compact, compressed
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('tinypng', function () {
    return gulp.src('./src/img/**/*.{png,jpg,jpeg}')
        .pipe(tinypng({
            key: '6_2Zvi5VbHQSrJKPfShhwUz5CjDdD97l',
            sigFile: './src/img/.tinypng-sigs',
            log: true
        }))
        .pipe(gulp.dest('./dist/img'));
});

gulp.task('htmlmin', function () {
    return gulp.src(['./src/**/*.html'])
        .pipe(htmlmin({
            collapseWhitespace: false,
            minifyCSS: true,
            removeComments: false
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('copy-files', function () {
    var copy = {
        files: ['./src/fonts/**/*']
    };
    return gulp.src(copy.files, {
            base: "./src/"
        })
        .pipe(gulp.dest('./dist'));
});

/**
 * Build
 */

gulp.task('build', ['sass', 'tinypng', 'htmlmin', 'copy-files'], function () {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch('./src/**/*.html', ['htmlmin']);
    gulp.watch('./src/**/*.php', ['copy-files']);
});